﻿
# Pre-requesites
# 1. download the nuget.exe https://www.nuget.org/downloads and copy to a local folder of your choice
# 2. download vs 2015 tools https://download.microsoft.com/download/E/E/D/EEDF18A8-4AED-4CE0-BEBE-70A83094FC5A/BuildTools_Full.exe
# 3. Need to copy Webapplication folder in the C:\Program Files (x86)\MSBuild\Microsoft\VisualStudio\v14.0 take folder from VS2017 0r VS2019
# 4. Verify all paths that matches your local enviroment and set accordingly

#$msbuild = "C:\Program Files (x86)\Microsoft Visual Studio\2019\Preview\MSBuild\Current\Bin\msbuild.exe" 
$msbuild = "C:\Program Files (x86)\MSBuild\14.0\Bin\msbuild.exe" 

$branchName ="Dev-CORE-ENG-2195"
$solutionRootFolder = "C:\projects"
$solutionFolder = "Xsolis.CaseManagement"
$solutionName = "Xsolis.CaseManagement.sln"
$solutionFullPath ="$solutionRootFolder\$branchName\$solutionFolder"
$packageLocation ="$solutionFullPath\packages"

#remove all nuget packages to get a clean install
Remove-Item -LiteralPath $packageLocation -Force -Recurse

# location of the ms build within your VS local installtion (this is preview 2019)
$solutionToBuild = "$solutionFullPath\$solutionName" 


$nugetLocation = "C:\Tools\nuget\nuget.exe"

& $nugetLocation restore $solutionToBuild

& $msbuild   $solutionToBuild   /p:RunOctoPack=true /p:DeployOnBuild=true /p:VisualStudioVersion=14.0 /p:RunCodeAnalysis=true /p:platform="any cpu" /p:configuration="release"

 