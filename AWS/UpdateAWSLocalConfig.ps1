﻿Add-Type -AssemblyName System.Windows.Forms
$keysTofind = 'aws_access_key_id=|aws_secret_access_key=|aws_session_token=';
$userName = (Get-WMIObject -ClassName Win32_ComputerSystem).Username;
$userName = Split-Path -Path $userName -Leaf;
$userName2 = $userName
if ($userName2.EndsWith("adm")) {
  $userName2 = $userName2.Substring(0, $userName2.Length - 3)
}
else {
  $userName2 = $userName + "adm"
}
$names = $userName, $userName2

[System.Windows.Forms.MessageBox]::Show("Are the AWS Credentials in your machine clipboard? Continue Task?", "Update AWS Configuration", "YesNo" , "Information" , "Button1");
Write-Host "This tool will update the AWS credentials with the ones that you copied from the AWS console from the console";

foreach ($name in $names) {
  Write-Host "Trying user name: " $name
  
  $awsCredentialFilePath = "C:\Users\$name\.aws\credentials";
  if (Test-Path -Path $awsCredentialFilePath -PathType Leaf) {

    Write-Host "Editing..."
    $keys = $keysTofind.Split("|");
    $awsConfigurationFileContent = Get-Content $awsCredentialFilePath;
    $splittedContent = $awsConfigurationFileContent -Split ([Environment]::NewLine);
    $dataInClipboard = Get-Clipboard;
    Write-Host "Starting update";
    # loop through the keys to process and replace
    for ($keyCounter = 0; $keyCounter -lt $keys.Length; $keyCounter++) {
      $currentCredential = $keys[$keyCounter];
      Write-Host "Processing:" $currentCredential;
      # read through the clipboard and get aws credentials
      for ($lineCounter = 0; $lineCounter -lt $dataInClipboard.Length; $lineCounter++) {
        $credentialFromClipboard = $dataInClipboard[$lineCounter];
        try {
          if ($credentialFromClipboard.StartsWith($currentCredential)) {
            for ($contentCounter = 0; $contentCounter -lt $splittedContent.Length; $contentCounter++) {
              $credentialValueToBeReplaced = $splittedContent[$contentCounter];
            
              if ($credentialValueToBeReplaced.StartsWith($currentCredential)) {
              (Get-Content $awsCredentialFilePath).replace($credentialValueToBeReplaced, $credentialFromClipboard ) | Set-Content $awsCredentialFilePath
              }
            }
          }
        }
        catch {
          Write-Host $_
          Write-Host "No Aws Credentials found in Clipboard";
          exit;
        }
      }
    }
  }
  else {
    Write-Host "Skipping..."
  }
}
Write-Host "Update completed!";
